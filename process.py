import ROOT
from .commonHelpers.logger import logger
from .commonHelpers.options import setAttributes, updateAttributes

class Process:


    defaults = dict(
        style = "background",
        color = None,
        lineColor = None,
        fillColor = None,
        markerColor = None,
        lineStyle =  None,
        lineWidth =  None,
        fillStyle = None,
        markerStyle = None,
        drawErrorBand = False,
        customLabel =  None,
        cut =  None,
        norm =  False,
        normToProcess = None,
        scale =  None,
        multiRatio =  False,
        legendTitle = None,
        drawLegend = True,
        drawString = "",
        varexp = None,
        ratioDenominatorProcess = None,
        stackOnTop = False,
        sysTag = None,
        noLumiNorm = False,
    )
    setAttributes = setAttributes
    updateAttributes = updateAttributes


    _doc_parameters = """
        Parameters to be used for :py:meth:`~MPF.plotStore.PlotStore.registerHist` for histograms created from the process:

        :param style: (default: "background")
        :param color: (default: None)
        :param lineColor: (default: None)
        :param markerColor: (default: None)
        :param fillColor: (default: None)
        :param lineStyle: (default: None)
        :param lineWidth: (default: None)
        :param fillStyle: (default: None)
        :param markerStyle: (default: None)
        :param drawErrorBand: (default: False)
        :param drawString: (default: None)
        :param legendTitle: (default: None)
        :param drawLegend: (default: True)
        :param ratioDenominatorProcess: (default: None)
        :param stackOnTop: (default: False)

        The other parameters:

        :param cut: cut/weight expression to be used only for this process
        :param norm: normalise the resulting histograms to unity?
        :param scale: scale resulting histogram by this factor
        :param varexp: use this varexp for this process **instead** of the one used for all the other processes

        :param normToProcess: normalise the histogram to the same
                              integral as the given process (by name) before plotting (only
                              used for hists of style "systematic" in :py:class:`~MPF.treePlotter.TreePlotter`)

        :param sysTag: name of the systematic variation (mainly for internal use in
                       :py:class:`~MPF.processProjector.ProcessProjector` and :py:class:`~MPF.treePlotter.TreePlotter`)

        :param noLumiNorm: don't normalise this process to the luminosity configured

    """


    def __init__(self, name, **kwargs):
        """
        Create a process to be used with
        :py:meth:`~MPF.processProjector.ProcessProjector`

        {parameters}

        """

        self.trees = []
        self.name = name
        self.hist = None
        self.bins = None
        self.yieldsDict = None
        self.setAttributes(self.defaults, **kwargs)
        logger.debug("Initialising process {}".format(self.name))
        logger.debug("Cut: {}".format(self.cut))

    __init__.__doc__ = __init__.__doc__.format(parameters=_doc_parameters)


    def addTree(self, filename, treename, cut=None):
        """
        Add a tree to the process.

        :param filename: path to the root file that contains the tree
        :param treename: name of the tree
        :param cut: optional cut/weight expression to be applied only for this tree

        """
        treeKwargs = dict(cut=cut)
        self.trees.append((filename, treename, treeKwargs))

    def setOptions(self, **kwargs):
        "Set options to new given values - reset the rest to defaults"
        self.setAttributes(self.defaults, **kwargs)

    def updateOptions(self, **kwargs):
        "Set options to new given values - leave the rest as they are"
        self.updateAttributes(self.defaults, **kwargs)

    def rawEvents(self):
        from .histProjector import HistProjector
        HP = HistProjector()
        nEvts = 0
        for path, treeName, treeKwargs in self.trees:
            cut = self.cut if self.cut is not None else "1"
            if treeKwargs["cut"] is not None:
                cut = "({})*({})".format(cut, treeKwargs["cut"])
            nEvts += HP.getYieldPath(treeName, cut, path)[0]
        return nEvts

    def __repr__(self):
        out = "<Process {}: ".format(self.name)
        out += "attributes: "
        for attribute in self.defaults:
            out += "{}={}, ".format(attribute, getattr(self, attribute))
        out += "hist: {} ".format(self.hist)
        out += ">"
        return out

