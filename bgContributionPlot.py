#!/usr/bin/env/python
"""
Similiar to :py:meth:`~MPF.dataMCRatioPlot.DataMCRatioPlot`, but
also shows the relative contributions of the background processes
in each bin in a 3rd pad.

Example
--------

.. literalinclude:: /../examples/bgContributionPlot.py

.. image::  images/bgContributionPlot.png
   :width: 600px

"""

from .plotStore import PlotStore
from . import pyrootHelpers as PH
from .errorBands import AE
from .line import Line
from .histograms import getHM
from .commonHelpers.logger import logger
logger = logger.getChild(__name__)

import ROOT

class BGContributionPlot(PlotStore):

    """

    :param noData: Don't show a data/MC ratio
    :param bgContributionPadTitle: Title on the pad showing the relative contributions

    Overwrites the defaults for the following :py:meth:`~MPF.plotStore.PlotStore` parameters:

    :param ignoreNumErrors: default: False
    :param ignoreDenErrors: default: True
    :param ratioMode: default: "rawpois"
    :param ratioUp: default: 2.25
    :param ratioDown: default: 0.25

    For further options see :py:meth:`~MPF.plotStore.PlotStore`
    """

    def __init__(self,
                 bgContributionPadTitle="Contributions",
                 noData=False,
                 **kwargs):
        self.bgContributionPadTitle = bgContributionPadTitle
        self.noData = noData
        kwargs = dict(
            ratioUp=kwargs.pop("ratioUp", 2.25),
            ratioDown=kwargs.pop("ratioDown", 0.25),
            ignoreNumErrors=kwargs.pop("ignoreNumErrors", False),
            ignoreDenErrors=kwargs.pop("ignoreDenErrors", True),
            ratioMode=kwargs.pop("ratioMode", "rawpois"),
            **kwargs
        )
        if self.noData:
            super(BGContributionPlot, self).__init__(splitting='ratio', **kwargs)
        else:
            super(BGContributionPlot, self).__init__(splitting='3pads', **kwargs)

    def saveAs(self, path, **kwargs):
        self.buildMainPad(**kwargs)
        if self.noData:
            bottomPad = self.canvas.pads['bottom']
            bottomPad.drawables.append(self.BGContributionHisto())

            bottomPad.yMinimum = 0.0
            bottomPad.yMaximum = 1.0
            bottomPad.yTitle = self.bgContributionPadTitle
        else:
            bottomPad = self.canvas.pads['bottom1']
            self.addDataMCRatio(bottomPad)

            bottomPad2 = self.canvas.pads['bottom2']
            bottomPad2.drawables.append(self.BGContributionHisto())

            bottomPad2.yMinimum = 0.0
            bottomPad2.yMaximum = 1.0
            bottomPad2.yTitle = self.bgContributionPadTitle

        return super(BGContributionPlot, self).saveAs(path, **kwargs)
