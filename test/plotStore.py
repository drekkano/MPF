import ROOT

from ..plot import Plot
from ..bgContributionPlot import BGContributionPlot
from ..significanceScanPlot import SignificanceScanPlot
from ..dataMCRatioPlot import DataMCRatioPlot
from ..signalRatioPlot import SignalRatioPlot
from ..efficiencyPlot import EfficiencyPlot
from .. import pyrootHelpers as PH
from .. import IOHelpers as IO
from .helpers.pdfCollectionTest import PDFCollectionTest
from ..examples.exampleHelpers import histogramFromList
from ..atlasStyle import setAtlasStyle
from ..globalStyle import useOptions

from ..commonHelpers.logger import logger
logger = logger.getChild(__name__)


pdfs = []

def tearDownModule():
    """Merge the pdfs of all tests here"""
    if len(pdfs) > 0:
        try:
            PH.pdfUniteOrMove(pdfs, "simpleHists.pdf")
            logger.warn("These tests that just ran require to look at the pdfs (simpleHists.pdf)")
        except IOError:
            logger.error("Couldn't merge pdfs: {}".format(pdfs))


class BaseTest(PDFCollectionTest):

    """Base class for all plotStore tests. Appends the merged pdfs to the
    module pdfs (which will finally also be merged in the end)
    """

    @classmethod
    def setUpClass(cls):
        super(BaseTest, cls).setUpClass()
        cls.mergedName = next(cls.pdfNames)

    @classmethod
    def tearDownClass(cls):
        super(BaseTest, cls).tearDownClass()
        if cls.imageFormat == "pdf":
            pdfs.append(cls.mergedName)


class PlotFromHist(BaseTest):

    def setUp(self):
        ROOT.gRandom.SetSeed(1234)
        fallingDistribution = [(float(100-i%100), float(i)) for i in range(500)]
        lessFallingDistribution = [(float(100-i%100), float(i*0.3+100)) for i in range(500)]
        risingDistribution = [(float(i%100), float(i)) for i in range(500)]
        for l in [fallingDistribution, lessFallingDistribution, risingDistribution]:
            l.append((0, 0))
            l.append((100, 0))
        self.histo1 = histogramFromList(fallingDistribution, 10)
        self.histo2 = histogramFromList(risingDistribution, 10)
        self.histo3 = histogramFromList(lessFallingDistribution, 10)
        self.options = dict()

    def tearDown(self):
        with useOptions(**self.options):
            self.pdf = self.plot.saveAs(next(self.pdfNames))
        super(PlotFromHist, self).tearDown()


    def test_signal_only(self):
        self.plot = Plot(debugText=["test_signal_only"])
        self.plot.registerHist(self.histo2, style='signal', color='kRed', legendTitle='Signal')

    def test_signal_only_processLabel(self):
        self.plot = Plot(customTexts=[(0.2, 0.2, self._testMethodName)], processLabel="This is a process label Z#rightarrow WW#rightarrow hA")
        self.plot.registerHist(self.histo2, style='signal', color='kRed', legendTitle='Signal')

    def test_signal_only_processLabel_scaled(self):
        self.histo2.Scale(10)
        self.plot = Plot(customTexts=[(0.2, 0.2, self._testMethodName)], processLabel="This is a process label Z#rightarrow WW#rightarrow hA")
        self.plot.registerHist(self.histo2, style='signal', color='kRed', legendTitle='Signal')

    def test_signal_only_processLabel_log(self):
        self.histo2.Scale(10)
        self.plot = Plot(customTexts=[(0.2, 0.2, self._testMethodName)], processLabel="This is a process label Z#rightarrow WW#rightarrow hA", logy=True)
        self.plot.registerHist(self.histo2, style='signal', color='kRed', legendTitle='Signal')

    def test_bkg_only(self):
        self.plot = Plot(debugText=["test_bkg_only"])
        self.plot.registerHist(self.histo1, style='background', color='kBlue', legendTitle='Background')

    def test_xtitle_unit(self):
        self.plot = Plot(xTitle="ducks", unit="feathers", debugText=["test_xtitle_unit"])
        self.plot.registerHist(self.histo1, style='background', color='kBlue', legendTitle='Background')

    def test_data_only(self):
        self.plot = Plot(debugText=["test_data_only"])
        self.plot.registerHist(self.histo3, style='data', legendTitle='Data')

    def test_data_only_poisson(self):
        self.options = dict(poissonIntervalDataErrors=True)
        self.histo3.Scale(0.001)
        # atm poisson errors won't be considered for calculating max with errors
        self.plot = Plot(debugText=[self._testMethodName], yMax=20)
        self.plot.registerHist(self.histo3, style='data', legendTitle='Data')

    def test_multi_data(self):
        self.plot = Plot(debugText=["test_multi_data"])
        self.plot.registerHist(self.histo1, style='data', legendTitle='Data 1', process="Data1")
        self.plot.registerHist(self.histo2, style='data', legendTitle='Data 2', process="Data2")

    def test_sig_bkg(self):
        self.plot = Plot(debugText=["test_sig_bkg"])
        self.plot.registerHist(self.histo1, style='background', color='kBlue', legendTitle='Background')
        self.plot.registerHist(self.histo2, style='signal', color='kRed', legendTitle='Signal')

    def test_sig_data(self):
        self.plot = Plot(debugText=["test_sig_data"])
        self.plot.registerHist(self.histo3, style='data', legendTitle='Data')
        self.plot.registerHist(self.histo2, style='signal', color='kRed', legendTitle='Signal')

    def test_data_bkg(self):
        self.plot = Plot(debugText=["test_data_bkg"])
        self.plot.registerHist(self.histo3, style='data', legendTitle='Data')
        self.plot.registerHist(self.histo1, style='background', color='kBlue', legendTitle='Signal')

    def test_sig_data_bkg(self):
        self.plot = Plot(debugText=["test_sig_data_bkg"])
        self.plot.registerHist(self.histo1, style='background', color='kBlue', legendTitle='Background')
        self.plot.registerHist(self.histo3, style='data', legendTitle='Data')
        self.plot.registerHist(self.histo2, style='signal', color='kRed', legendTitle='Signal')

    def test_multi_bkg(self):
        self.histo1.Scale(0.5)
        self.plot = Plot(debugText=["test_multi_bkg"])
        self.plot.registerHist(self.histo1, style='background', color='kBlue', legendTitle='Background 1')
        self.plot.registerHist(self.histo1, style='background', color='kGreen', legendTitle='Background 2')
        self.plot.registerHist(self.histo3, style='data', legendTitle='Data')
        self.plot.registerHist(self.histo2, style='signal', color='kRed', legendTitle='Signal')

    def test_multi_bkg_noLinesForBkg(self):
        self.options = dict(noLinesForBkg=True)
        self.histo1.Scale(0.5)
        self.plot = Plot(debugText=[self._testMethodName])
        self.plot.registerHist(self.histo1, style='background', color='kBlue', legendTitle='Background 1')
        self.plot.registerHist(self.histo1, style='background', color='kGreen', legendTitle='Background 2')
        self.plot.registerHist(self.histo3, style='data', legendTitle='Data')
        self.plot.registerHist(self.histo2, style='signal', color='kRed', legendTitle='Signal')

    def test_significance_scan_plotStore(self):
        self.histo1.Scale(0.5)
        self.plot = SignificanceScanPlot(drawCumulativeStack=True, debugText=["significance_scan"])
        self.plot.registerHist(self.histo1, style='background', color='kBlue', legendTitle='Background 1')
        self.plot.registerHist(self.histo1, style='background', color='kGreen', legendTitle='Background 2')
        self.plot.registerHist(self.histo2, style='signal', color='kRed', legendTitle='Signal')

    def test_significance_scan_plotStore_ratioLimits(self):
        self.histo1.Scale(0.5)
        self.plot = SignificanceScanPlot(debugText=[self._testMethodName], ratioUp=23, ratioDown=10)
        self.plot.registerHist(self.histo1, style='background', color='kBlue', legendTitle='Background 1')
        self.plot.registerHist(self.histo1, style='background', color='kGreen', legendTitle='Background 2')
        self.plot.registerHist(self.histo2, style='signal', color='kRed', legendTitle='Signal')

    def test_stack_on_top(self):
        self.histo1.Scale(0.5)
        self.plot = SignificanceScanPlot(debugText=["stack_on_top"])
        self.plot.registerHist(self.histo1, style='background', color='kBlue', legendTitle='Background 1')
        self.plot.registerHist(self.histo1, style='background', color='kGreen', legendTitle='Background 2')
        myGaus = ROOT.TF1("myGaus", "TMath::Gaus(x, 60,10)")
        gaussHist = ROOT.TH1F(next(PH.tempNames), "", 10, 0, 100)
        gaussHist.FillRandom("myGaus", 10000)
        self.plot.registerHist(gaussHist, style='signal', color='kRed', legendTitle='Signal', stackOnTop=True)

    def test_stack_on_top_filled(self):
        self.histo1.Scale(0.5)
        self.plot = SignificanceScanPlot(debugText=["stack_on_top_filled"])
        self.plot.registerHist(self.histo1, style='background', color='kBlue', legendTitle='Background 1')
        self.plot.registerHist(self.histo1, style='background', color='kGreen', legendTitle='Background 2')
        myGaus = ROOT.TF1("myGaus", "TMath::Gaus(x, 60,10)")
        gaussHist = ROOT.TH1F(next(PH.tempNames), "", 10, 0, 100)
        gaussHist.FillRandom("myGaus", 10000)
        self.plot.registerHist(gaussHist, style='signal', color='kRed', legendTitle='Signal', stackOnTop=True, fillStyle=1001)

    def test_stack_on_top_as_background(self):
        self.histo1.Scale(0.5)
        self.plot = Plot(debugText=["stack_on_top_as_background"])
        self.plot.registerHist(self.histo1, style='background', color='kBlue', legendTitle='Background 1')
        self.plot.registerHist(self.histo1, style='background', color='kGreen', legendTitle='Background 2')
        myGaus = ROOT.TF1("myGaus", "TMath::Gaus(x, 60,10)")
        gaussHist = ROOT.TH1F(next(PH.tempNames), "", 10, 0, 100)
        gaussHist.FillRandom("myGaus", 10000)
        self.plot.registerHist(gaussHist, style='background', color='kRed', legendTitle='Signal', stackOnTop=True)

    def test_bkg_contributions(self):
        self.histo1.Scale(0.5)
        self.plot = BGContributionPlot(debugText=["test_bkg_contributions"])
        self.plot.registerHist(self.histo1, style='background', color='kBlue', legendTitle='Background 1')
        bkg2 = self.histo2.Clone(next(PH.tempNames))
        bkg2.Scale(0.1)
        self.plot.registerHist(bkg2, style='background', color='kGreen', legendTitle='Background 2')
        self.plot.registerHist(self.histo3, style='data', legendTitle='Data')

    def test_data_mc_ratio(self):
        self.plot = DataMCRatioPlot(debugText=["test_data_mc_ratio"])
        self.histo1.Scale(0.5*1.3)
        self.plot.registerHist(self.histo1, style='data', legendTitle='Data')
        self.plot.registerHist(self.histo3, style='background', color='kBlue', legendTitle='Background')

    def test_data_mc_ratio_arrow(self):
        self.plot = DataMCRatioPlot(ratioUp=1.25, ratioDown=0.75, ratioMode="pois",
                                    debugText=["test_data_mc_ratio_arrow"])
        self.histo1.Scale(0.5*0.9*1.3)
        self.plot.registerHist(self.histo1, style='data', legendTitle='Data')
        self.plot.registerHist(self.histo3, style='background', color='kBlue', legendTitle='Background')

    def test_bkg_lots_of_text(self):
        self.plot = Plot(debugText=[self._testMethodName],
                         customTexts=[
                             (0.2, 0.7, "hans"),
                             (0.5, 0.6, "horst"),
                             (0.9, 0.2, "mehl"),
                             (0.7, 0.75, "muschelmusik"),
                         ])
        self.plot.registerHist(self.histo1, style='background', color='kBlue', legendTitle='Background')



class PlotEfficiency(BaseTest):

    @classmethod
    def setUpClass(cls):
        setAtlasStyle()
        ROOT.gRandom.SetSeed(1234)
        cls.passed, cls.total = cls.createTotalPassedHist("")
        cls.passed_data, cls.total_data = cls.createTotalPassedHist("_data")
        super(PlotEfficiency, cls).setUpClass()


    @staticmethod
    def createTotalPassedHist(name, nPassedHists=1):
        setAtlasStyle()
        passed = []
        for i in range(nPassedHists):
            h = ROOT.TH1F("passed_{}_{}".format(i, name), "", 50, 100, 300)
            passed.append(h)
            h.Sumw2()
        total = ROOT.TH1F("total_{}".format(name), "", 50, 100, 300)
        total.Sumw2()
        for i in range(10000):
            val = ROOT.gRandom.Gaus(100, 200)
            total.Fill(val)
            for passedHist in passed:
                shift = ROOT.gRandom.Gaus(0, 20)
                if (val+shift) > 200:
                    passedHist.Fill(val)
        if nPassedHists == 1:
            return passed[0], total
        else:
            return passed, total


    def tearDown(self):
        self.pdf = self.plot.saveAs(next(self.pdfNames))
        super(PlotEfficiency, self).tearDown()


    def test_efficiency(self):
        self.plot = EfficiencyPlot(debugText=["test_efficiency"])
        self.plot.registerHist(self.total, style='signal')
        self.plot.registerHist(self.passed, style='signal', legendTitle="Efficiency")

    def test_efficiency_bayes(self):
        self.plot = EfficiencyPlot(debugText=["test_efficiency_bayes"], ratioMode="bayes")
        self.plot.registerHist(self.total, style='signal')
        self.plot.registerHist(self.passed, style='signal', legendTitle="Efficiency")

    def test_efficiency_datamc(self):
        self.plot = EfficiencyPlot(debugText=["test_efficiency_datamc"], ratioMode="bayes")
        self.plot.registerHist(self.total, style='signal')
        self.plot.registerHist(self.passed, style='signal', legendTitle="MC")
        self.plot.registerHist(self.total_data, style='data')
        self.plot.registerHist(self.passed_data, style='data', legendTitle="Data")

    def test_efficiency_datamc_ratio(self):
        self.plot = EfficiencyPlot(debugText=["test_efficiency_datamc_ratio"], ratioMode="bayes", doDataMCRatio=True)
        self.plot.registerHist(self.total, style='signal')
        self.plot.registerHist(self.passed, style='signal', legendTitle="MC")
        self.plot.registerHist(self.total_data, style='data')
        self.plot.registerHist(self.passed_data, style='data', legendTitle="Data")

    def test_efficiency_datamc_ratio_multiple(self):
        passed, total = self.createTotalPassedHist("", 2)
        self.plot = EfficiencyPlot(debugText=["test_efficiency_datamc_ratio_multiple"], ratioMode="bayes", doDataMCRatio=True)
        self.plot.registerHist(total, style='signal')
        self.plot.registerHist(passed[0], style='signal', legendTitle="MC")
        self.plot.registerHist(passed[1], style='signal', legendTitle="MC2")
        self.plot.registerHist(self.total_data, style='data')
        self.plot.registerHist(self.passed_data, style='data', legendTitle="Data")

    def test_efficiency_multiple_targetDenominator(self):
        passed, total = self.createTotalPassedHist("1", 1)
        passed2, total2 = self.createTotalPassedHist("2", 1)
        self.plot = EfficiencyPlot(debugText=["test_efficiency_multiple_targetDenominator"])
        self.plot.registerHist(total, style='signal', process="total1")
        self.plot.registerHist(total2, style='signal', process="total2")
        self.plot.registerHist(passed, style='signal', ratioDenominatorProcess="total1", legendTitle="MC1")
        self.plot.registerHist(passed2, style='signal', ratioDenominatorProcess="total2", legendTitle="MC2")


class PlotFromHistFromFile(BaseTest):

    testHistPath = "/tmp/testHist.root"

    def setUp(self):
        setAtlasStyle()
        ROOT.gRandom.SetSeed(1234)
        self.histName = next(PH.tempNames)
        self.hist = ROOT.TH1F(self.histName, "", 100, -5, 5)
        self.hist.FillRandom("gaus")
        with IO.ROpen(self.testHistPath, "RECREATE") as f:
            f.cd()
            self.hist.Write()

    def tearDown(self):
        self.pdf = self.plot.saveAs(next(self.pdfNames))
        super(PlotFromHistFromFile, self).tearDown()

    def test_register_from_file_with_ROpen(self):
        self.plot = Plot(debugText=[self._testMethodName])
        with IO.ROpen(self.testHistPath) as f:
            self.plot.registerHist(f.Get(self.histName), style="background")
