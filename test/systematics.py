import unittest
import math

import ROOT

from .. import pyrootHelpers as PH
from ..histograms import getHM
from ..commonHelpers.logger import logger
logger = logger.getChild(__name__)

class Test(unittest.TestCase):

    def assertEqualFloat(self, f1, f2):
        logger.debug("{} ?= {}".format(f1, f2))
        self.assertTrue(abs(f1-f2) <= 1e-9*abs(f1))

    def setUp(self):
        self.hist_nom = ROOT.TH1D(next(PH.tempNames), "", 10, 0, 10)

        for i in range(0, 12):
            self.hist_nom.SetBinContent(i, 1)
            self.hist_nom.SetBinError(i, 0)

        self.hist_syst1 = ROOT.TH1D(next(PH.tempNames), "", 10, 0, 10)

        for i in range(0, 12):
            self.hist_syst1.SetBinContent(i, 1+i*0.1)

    def test_singleHist_largest(self):

        hm = getHM(self.hist_nom)

        hm.addSystematicError(self.hist_syst1, mode="largest")

        for i in range(0, 12):
            self.assertEqual(hm.GetBinContent(i), 1)
            self.assertEqualFloat(hm.GetBinError(i), i*0.1)

    def test_singleHist_symUpDown(self):

        hm = getHM(self.hist_nom)

        hm.addSystematicError(self.hist_syst1, mode="symUpDown")

        for i in range(0, 12):
            self.assertEqual(hm.GetBinContent(i), 1)
            self.assertEqualFloat(hm.GetBinError(i), 0.5*i*0.1)

        hm.addSystematicError(self.hist_syst1, mode="symUpDown")

        for i in range(0, 12):
            self.assertEqual(hm.GetBinContent(i), 1)
            self.assertEqualFloat(hm.GetBinError(i), math.sqrt(2)*0.5*i*0.1)
