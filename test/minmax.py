import unittest
import itertools
import shutil
import os

import ROOT

from ..plot import Plot
from .. import pyrootHelpers as PH
from ..examples.exampleHelpers import createExampleTrees
from ..treePlotter import TreePlotter
from ..process import Process
from ..pyrootHelpers import TangoColors as tcol
from .helpers.pdfCollectionTest import PDFCollectionTest

from ..commonHelpers.logger import logger
logger = logger.getChild(__name__)

class Test(PDFCollectionTest):


    @classmethod
    def setUpClass(cls):
        cls.testTreePath = "/tmp/testTreeMPF.root"
        if not os.path.exists(cls.testTreePath):
            logger.info("creating example tree {}".format(cls.testTreePath))
            createExampleTrees(cls.testTreePath)
        cls.mergedName = "topMarginTests.pdf"
        PDFCollectionTest.setUpClass()


    def setUp(self):
        self.hist = ROOT.TH1D(next(PH.tempNames), "", 2, 0, 2)
        self.histpm = ROOT.TH1D(next(PH.tempNames), "", 2, 0, 2)
        self.errhist = ROOT.TH1D(next(PH.tempNames), "", 2, 0, 2)
        self.hugeerrhist = ROOT.TH1D(next(PH.tempNames), "", 2, 0, 2)
        self.hist.SetBinContent(1, 1512)
        self.hist.SetBinContent(2, 1512)
        self.histpm.SetBinContent(1, -1512)
        self.histpm.SetBinContent(2, 1512)
        self.errhist.SetBinContent(1, 1512)
        self.errhist.SetBinError(1, 112)
        self.errhist.SetBinContent(2, 1512)
        self.errhist.SetBinError(2, 256)
        self.hugeerrhist.SetBinContent(1, 1512)
        self.hugeerrhist.SetBinError(1, 1000)
        self.hugeerrhist.SetBinContent(2, 1512)
        self.hugeerrhist.SetBinError(2, 1000)


    def test_lin1(self):
        p = Plot(debugText=["Lin scale 1 pad"])
        p.registerHist(self.histpm, style="signal")
        p.registerHist(self.hist, style="background")
        self.pdf = p.saveAs(next(self.pdfNames))

    def test_log1(self):
        p = Plot(logy=True, debugText=["Log scale 1 pad"])
        p.registerHist(self.histpm, style="signal")
        p.registerHist(self.hist, style="background")
        self.pdf = p.saveAs(next(self.pdfNames))

    def test_log1_smallmax(self):
        p = Plot(logy=True, debugText=["Log scale 1 pad - small values"])
        self.histpm.Scale(0.000001)
        self.hist.Scale(0.000001)
        p.registerHist(self.histpm, style="signal")
        p.registerHist(self.hist, style="background")
        self.pdf = p.saveAs(next(self.pdfNames))

    def test_lin2(self):
        p = Plot(splitting="ratio", debugText=["Lin scale 2 pads"])
        p.registerHist(self.histpm, style="signal")
        p.registerHist(self.hist, style="background")
        self.pdf = p.saveAs(next(self.pdfNames))

    def test_log2(self):
        p = Plot(splitting="ratio", logy=True, debugText=["Log scale 2 pads"])
        p.registerHist(self.histpm, style="signal")
        p.registerHist(self.hist, style="background")
        self.pdf = p.saveAs(next(self.pdfNames))

    def test_lin3(self):
        p = Plot(splitting="3pads", debugText=["Lin scale 3 pads"])
        p.registerHist(self.histpm, style="signal")
        p.registerHist(self.hist, style="background")
        self.pdf = p.saveAs(next(self.pdfNames))

    def test_log3(self):
        p = Plot(splitting="3pads", logy=True, debugText=["Log scale 3 pads"])
        p.registerHist(self.histpm, style="signal")
        p.registerHist(self.hist, style="background")
        self.pdf = p.saveAs(next(self.pdfNames))

    def test_treePlotter(self):
        tp = TreePlotter(cut="ht>1000", varexp="met", xTitle="E_{T}^{miss}", unit="GeV",
                         plotType="DataMCRatioPlot", ratioUp=1.25, ratioDown=0.75)
        tp.addProcessTree("bkg1", self.testTreePath, "w1", cut="(1-50*(1-w))", style="background", color=tcol.blue2)
        tp.addProcessTree("bkg2", self.testTreePath, "w2", style="background", color=tcol.green2)
        tp.addProcessTree("sig", self.testTreePath, "w4", style="signal", color=tcol.red2)
        data = Process("data", style="data")
        data.addTree(self.testTreePath, "w1")
        data.addTree(self.testTreePath, "w2")
        tp.addProcess(data)
        self.pdf = tp.plot(next(self.pdfNames), cut="met>200",
                           varexp="ht", xTitle="H_{T}", xmin=0., xmax=3000, nbins=100)


    def test_linNoMarg(self):
        p = Plot(logy=False, debugText=["Lin no insideTopMargin"], insideTopMargin=None)
        p.registerHist(self.histpm, style="signal")
        p.registerHist(self.hist, style="background")
        self.pdf = p.saveAs(next(self.pdfNames))


    def test_logNoMarg(self):
        p = Plot(logy=True, debugText=["Log no insideTopMargin"], insideTopMargin=None)
        p.registerHist(self.histpm, style="signal")
        p.registerHist(self.hist, style="background")
        self.pdf = p.saveAs(next(self.pdfNames))


    def test_hugeError(self):
        p = Plot(debugText=["huge error"])
        p.registerHist(self.hugeerrhist, style="background")
        self.pdf = p.saveAs(next(self.pdfNames))


    def test_customRange(self):
        p = Plot(logy=True, debugText=["Custom range: 1e-10 - 10"], yMin=1e-10, yMax=10)
        p.registerHist(self.histpm, style="signal")
        p.registerHist(self.hist, style="background")
        self.pdf = p.saveAs(next(self.pdfNames))
