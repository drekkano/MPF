import unittest
import itertools
import os
import shutil

from matplotlib.testing.compare import convert, compare_images

from ... import pyrootHelpers as PH
from ...atlasStyle import setAtlasStyle
from ...commonHelpers.logger import logger
logger = logger.getChild(__name__)

globalCounter = itertools.count()

class PDFCollectionTest(unittest.TestCase):

    testNames = set()

    imageFormat = "png"

    """Base class for tests that create one pdf per test.
    The classes have to implement a setUpClass method that defines the name of the
    merged pdf. For example::

      @classmethod
      setUpClass(cls):
          cls.mergedName = "myTests.pdf"
          super(myClass, cls).setUpClass()

    """

    def tearDown(self):
        try:
            self.testImages
        except AttributeError:
            self.testImages = [self.pdf]
        if len(self.testImages) == 0:
            self.testImages = [self.pdf]
        if not self._testMethodName in self.testNames:
            self.testNames.add(self._testMethodName)
        else:
            raise Exception("Found duplicated test name: {}. You "
                            "must have unique test names for the "
                            "image comparison tests!".format(self._testMethodName))
        comparisons = []
        for i, imageName in enumerate(self.testImages):
            if imageName is None:
                continue
            self.pdfs.append(imageName)
            if self.imageFormat == "pdf":
                continue
            suffix = ""
            if len(self.testImages) > 1:
                suffix = "_{}".format(i)
            oldreference = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                        "../references/{}{}.png".format(self._testMethodName, suffix))
            newreference = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                        "../references/{}{}__new.png".format(self._testMethodName, suffix))
            if not imageName.endswith(".png"):
                convert(imageName, False)
                shutil.move(imageName.replace(".pdf", "_pdf.png"), newreference)
            else:
                shutil.move(imageName, newreference)
            if os.path.exists(oldreference):
                # store the match result (so we have all new images in case one fails)
                comparisons.append(compare_images(oldreference, newreference, 0.001))
            else:
                logger.warning("Can't find reference image {} - won't do an automatic image comparison!".format(oldreference))
        # check if all images matched
        for cmpOutput in comparisons:
            self.assertEqual(cmpOutput, None)


    @classmethod
    def setUpClass(cls):
        cls.pdfs = []
        cls.pdfNames = ("/tmp/test{}.{}".format(i, cls.imageFormat) for i in globalCounter)
        setAtlasStyle()

    @classmethod
    def tearDownClass(cls):
        if cls.imageFormat == "pdf":
            logger.warn("These tests that just ran require to look at the pdfs ({})".format(cls.mergedName))
            if len(cls.pdfs) < 2:
                logger.info("Renaming {} to {}".format(cls.pdfs[0], cls.mergedName))
                shutil.move(cls.pdfs[0], cls.mergedName)
                cls.pdfs = []
            else:
                isMerged = PH.pdfUnite(cls.pdfs, cls.mergedName)
                if not isMerged:
                    # don't delete in this case
                    cls.pdfs = []
            for pdf in cls.pdfs:
                os.unlink(pdf)
