#!/usr/bin/env python

import ROOT
from MPF.significanceScanPlot import SignificanceScanPlot
from MPF.atlasStyle import setAtlasStyle

setAtlasStyle()
bkg1 = ROOT.TH1F("bkg1", "", 20, -2, 4)
bkg2 = ROOT.TH1F("bkg2", "", 20, -2, 4)
data = ROOT.TH1F("data", "", 20, -2, 4)
signal = ROOT.TH1F("signal", "", 20, -2, 4)
signal2 = ROOT.TH1F("signal2", "", 20, -2, 4)

bkg1.FillRandom("gaus")
bkg1.Scale(0.01)
bkg2.FillRandom("gaus")
bkg2.Scale(0.01)
shiftGaus = ROOT.TF1("shiftGaus", "TMath::Gaus(x, 2)")
signal.FillRandom("shiftGaus")
signal.Scale(0.003)
shiftGaus2 = ROOT.TF1("shiftGaus2", "TMath::Gaus(x, 1.5, 0.5)")
signal2.FillRandom("shiftGaus2")
signal2.Scale(0.005)

p = SignificanceScanPlot(xTitle="x")
p.registerHist(bkg1, style="background", process="Bkg 1", color="kGray+1")
p.registerHist(bkg2, style="background", process="Bkg 2", color="kGray+2")
p.registerHist(signal, style="signal", process="Signal 1", color="kRed", lineWidth=3)
p.registerHist(signal2, style="signal", process="Signal 2", color="kPink+1", lineWidth=3)
p.saveAs("plot.pdf")
