#!/usr/bin/env python

import ROOT
from MPF.dataMCRatioPlot import DataMCRatioPlot
from MPF.atlasStyle import setAtlasStyle

setAtlasStyle()
bkg1 = ROOT.TH1F("bkg1", "", 100, -2, 5)
bkg2 = ROOT.TH1F("bkg2", "", 100, -2, 5)
data = ROOT.TH1F("data", "", 100, -2, 5)
signal = ROOT.TH1F("signal", "", 100, -2, 5)

bkg1.FillRandom("gaus")
bkg1.Scale(0.5)
bkg2.FillRandom("gaus")
bkg2.Scale(0.5)
data.FillRandom("gaus")
signal.FillRandom("landau")

p = DataMCRatioPlot(xTitle="x")
p.registerHist(bkg1, style="background", process="Bkg 1")
p.registerHist(bkg2, style="background", process="Bkg 2")
p.registerHist(data, style="data", process="Data")
p.registerHist(signal, style="signal", process="Signal")
p.saveAs("plot.pdf")
