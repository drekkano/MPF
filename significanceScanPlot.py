#!/usr/bin/env/python
"""
Similiar to :py:meth:`~MPF.plot.Plot`, but also shows a
significance scan for all added signals in the second pad.

For further options see :py:meth:`~MPF.plotStore.PlotStore`


Example
--------

.. literalinclude:: /../examples/significanceScanPlot.py

.. image::  images/significanceScanPlot.png
   :width: 600px

"""

from .plotStore import PlotStore
import ROOT
from .commonHelpers.logger import logger
logger = logger.getChild(__name__)

class SignificanceScanPlot(PlotStore):

    def __init__(self,
                 drawCumulativeStack=False,
                 drawBackgroundContributionHisto=False,
                 significancePadTitle="Z",
                 significanceMode="BinomialExpZ",
                 significanceFunction=None,
                 cumulativeStackTitle="Cumulative",
                 scanDirection="forward",
                 processesOfInterest=None,
                 **kwargs):

        """
        Overwrites the defaults for the following :py:meth:`~MPF.plotStore.PlotStore` parameters:

        :param ratioUp: If set, used for the y-Axis maximum on the scan pad default: None
        :param ratioDown: If set, used for the y-Axis minimum on the scan pad default: None


        Plot specific options:

        :param significancePadTitle: y-Axis title for the significance pad
        :param drawCumulativeStack: draw the stack of cumulative distributions in a 3rd pad
        :param drawBackgroundContributionHisto: draw the relative background contributions in a 3rd pad
        :param cumulativeStackTitle: title for the cumulative distribution pad if given
        :param significanceMode/significanceFunction: see :py:meth:`~MPF.plotStore.PlotStore.getSignificanceHistogram`
        :param scanDirection: [forward, backwards, both]
        :param processesOfInterest: list of process names to be considered as signal, None defaults to all signal processes
        """

        self.drawCumulativeStack = drawCumulativeStack
        self.drawBackgroundContributionHisto = drawBackgroundContributionHisto
        self.significancePadTitle = significancePadTitle
        self.significanceMode = significanceMode
        self.significanceFunction = significanceFunction
        self.cumulativeStackTitle = cumulativeStackTitle
        self.scanDirection = scanDirection
        self.processesOfInterest = processesOfInterest

        if self.drawCumulativeStack or self.drawBackgroundContributionHisto:
            super(SignificanceScanPlot, self).__init__(splitting='3pads',
                                                       ratioUp=kwargs.pop("ratioUp", None),
                                                       ratioDown=kwargs.pop("ratioDown", None),
                                                       **kwargs)
        else:
            super(SignificanceScanPlot, self).__init__(splitting='ratio',
                                                       ratioUp=kwargs.pop("ratioUp", None),
                                                       ratioDown=kwargs.pop("ratioDown", None),
                                                       **kwargs)

    def saveAs(self, path, **kwargs):
        self.buildMainPad(**kwargs)

        if self.drawCumulativeStack or self.drawBackgroundContributionHisto:
            bottomPad = self.canvas.pads['bottom1']
        else:
            bottomPad = self.canvas.pads['bottom']
        self.addSignificanceScan(bottomPad,
                                 mode=self.significanceMode,
                                 customFunction=self.significanceFunction,
                                 processesOfInterest=self.processesOfInterest,
                                 scanDirection=self.scanDirection)
        bottomPad.yTitle = self.significancePadTitle

        if self.drawCumulativeStack:
            bottomPad2 = self.canvas.pads['bottom2']
            bottomPad2.logy = True
            self.addCumulativeStack(bottomPad2, forward=False)
            bottomPad2.yTitle = self.cumulativeStackTitle

        if self.drawBackgroundContributionHisto:
            bottomPad2 = self.canvas.pads['bottom2']
            bottomPad2.logy = False
            bottomPad2.drawables.append(self.BGContributionHisto())
            bottomPad2.yMinimum = 0.0
            bottomPad2.yMaximum = 1.0

        return super(SignificanceScanPlot, self).saveAs(path, **kwargs)
